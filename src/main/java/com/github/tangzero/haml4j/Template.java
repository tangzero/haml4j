package com.github.tangzero.haml4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.IOUtils;

/**
 * Class utilized for rendering a Haml template.
 */
public class Template {

	private static final Object HAML4J_ENGINE_CLASS = Container.getInstance().run("haml4j.rb");
	private Object engine;

	/**
	 * Constructs a template based on a .haml file.
	 * @param file Haml template.
	 * @throws IOException If the template does not exist.
	 */
	public Template(File file) throws IOException {
		String template = IOUtils.toString(new FileInputStream(file));
		engine = Container.getInstance().call(HAML4J_ENGINE_CLASS, "new", template);
	}

	/**
	 * Renders the template based on the values of a context.
	 * @param context Context values
	 * @return Template output.
	 */
	public String render(Map<String, Object> context) {
		return Container.getInstance().call(engine, "render", context);
	}

}
