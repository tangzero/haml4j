package com.github.tangzero.haml4j;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

/**
 * Basic Servlet that renders Haml templates.
 */
public class Haml4JServlet extends HttpServlet {

	private static final long serialVersionUID = -8974479395920825184L;
	public static final String TEMPLATE_PATH = "TemplatePath";

	private File templateDir;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#init(javax.servlet.ServletConfig)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		String templatePath = config.getInitParameter(TEMPLATE_PATH);
		templateDir = new File(config.getServletContext().getRealPath(templatePath));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		Map<String, Object> context = extractContext(request, response);
		Template template = new Template(loadTemplateFile(request));
		String output = template.render(context);
		IOUtils.write(output, response.getOutputStream());
	}

	/**
	 * Extract all attributes and others important things from request and
	 * response.
	 * 
	 * @param request
	 *            Request.
	 * @param response
	 *            REsposnse.
	 * @return Context with values.
	 */
	@SuppressWarnings("unchecked")
	protected Map<String, Object> extractContext(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> context = new HashMap<String, Object>();

		Enumeration<String> attributes = request.getAttributeNames();
		while (attributes.hasMoreElements()) {
			String attribute = attributes.nextElement();
			context.put(attribute, request.getAttribute(attribute));
		}

		context.put("base", request.getContextPath());
		context.put("params", request.getParameterMap());
		context.put("request", request);
		context.put("response", response);
		return context;
	}

	/**
	 * Load template file.
	 * 
	 * @param request
	 *            Request.
	 * @return Templaet file.
	 * @throws IOException
	 *             If the template does not exist.
	 */
	protected File loadTemplateFile(HttpServletRequest request) throws IOException {
		String template = request.getServletPath();
		return new File(templateDir, template);
	}

}
