package com.github.tangzero.haml4j;

import org.jruby.RubyInstanceConfig.CompileMode;
import org.jruby.embed.LocalContextScope;
import org.jruby.embed.LocalVariableBehavior;
import org.jruby.embed.PathType;
import org.jruby.embed.ScriptingContainer;

/**
 * Used to execute Ruby script.
 */
public class Container {

	private static Container instance = new Container();
	private ScriptingContainer scriptingContainer;

	/**
	 * Private constructor.
	 */
	private Container() {
		scriptingContainer = new ScriptingContainer(LocalContextScope.THREADSAFE, LocalVariableBehavior.TRANSIENT);
		scriptingContainer.setCompileMode(CompileMode.JIT);
	}

	/**
	 * Returns the singleton instance.
	 * 
	 * @return The instance.
	 */
	public static Container getInstance() {
		return instance;
	}

	/**
	 * Execute a script in .rb file.
	 * 
	 * @param filename
	 *            The filename of script.
	 * @return Script output.
	 */
	public Object run(String filename) {
		return scriptingContainer.runScriptlet(PathType.CLASSPATH, filename);
	}

	/**
	 * Invokes a method an Ruby object.
	 * 
	 * @param <Type>
	 *            Return type.
	 * @param object
	 *            Ruby object.
	 * @param method
	 *            Method name.
	 * @param args
	 *            Method return.
	 */
	@SuppressWarnings("unchecked")
	public <Type> Type call(Object object, String method, Object... args) {
		return (Type) scriptingContainer.callMethod(object, method, args, Object.class);
	}

}
