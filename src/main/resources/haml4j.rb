require "haml"

module Haml4J
	class Engine < Haml::Engine
		
		def render context
			scope = Object.new
			context.entrySet.each do |entry|
				scope.instance_variable_set "@#{entry.getKey}", entry.getValue
			end		
			super scope
		end
		
	end
end

Haml4J::Engine