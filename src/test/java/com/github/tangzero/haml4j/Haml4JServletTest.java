package com.github.tangzero.haml4j;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;


/**
 * Tests for {@link Haml4JServlet}
 */
public class Haml4JServletTest {

	private Haml4JServlet servlet;
	private ByteArrayOutputStream outputStream;
	private String templateName = "/posts/index.haml";
	private String contextPath = "/";
	
	@Mock private ServletConfig servletConfig;
	@Mock private ServletContext servletContext;
	@Mock private HttpServletRequest request;
	@Mock private HttpServletResponse response;
	@Mock private Map<String, String> parameters;
	
	@Before
	public void setup() throws Exception {
		initMocks(this);
		
		outputStream = new ByteArrayOutputStream();

		when(servletConfig.getInitParameter(Haml4JServlet.TEMPLATE_PATH)).thenReturn(contextPath);
		when(servletConfig.getServletContext()).thenReturn(servletContext);
		when(servletContext.getRealPath(contextPath)).thenReturn(getTemplatePath());
		when(request.getServletPath()).thenReturn(templateName);
		when(request.getContextPath()).thenReturn(contextPath);
		when(request.getParameterMap()).thenReturn(parameters);
		when(response.getOutputStream()).thenReturn(new ServletOutputStream() {
			@Override
			public void write(int b) throws IOException {
				outputStream.write(b);
			}
		});
		
		servlet = new Haml4JServlet();
		servlet.init(servletConfig);
	}
	
	@Test
	public void shouldLoadCorrectTemplateFile() throws Exception {
		File expected = new File(getTemplatePath() + templateName);
		File templateFile = servlet.loadTemplateFile(request);
		
		assertEquals("Should return correct template file.", expected, templateFile);
	}
	
	@Test
	public void shouldRenderATemplate() throws Exception {
		servlet = spy(servlet);
		doReturn(createContext()).when(servlet).extractContext(request, response);
		
		servlet.service(request, response);
		
		String output = new String(outputStream.toByteArray());
		assertEquals("Should render a template.", "<h2>Posts by tangzero</h2>", output.trim());
	}
	
	@Test
	public void shoudlExtractAContext() {
		when(request.getAttributeNames()).thenReturn(new StringTokenizer("username email"));
		when(request.getAttribute("username")).thenReturn("tangzero");
		when(request.getAttribute("email")).thenReturn("t4ngz3r0@gmail.com");
		
		Map<String, Object> context = servlet.extractContext(request, response);
		
		assertEquals("Should return a username.", context.get("username"), "tangzero");
		assertEquals("Should return a email.", context.get("email"), "t4ngz3r0@gmail.com");
		assertEquals("Should return a base path.", context.get("base"), contextPath);
		assertEquals("Should return a request parameters.", context.get("params"), parameters);
		assertEquals("Should return a http request.", context.get("request"), request);
		assertEquals("Should return a http response.", context.get("response"), response);
	}
		
	private Map<String, Object> createContext() {
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("username", "tangzero");
		return context;
	}

	private String getTemplatePath() {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		return new File(loader.getResource("template.haml").getPath()).getParent();
	}

}
