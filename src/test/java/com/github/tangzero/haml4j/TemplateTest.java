package com.github.tangzero.haml4j;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests for {@link Template}
 */
public class TemplateTest {

	private Template template;
	
	@Before
	public void setup() throws IOException {
		File file = new File(Thread.currentThread().getContextClassLoader().getResource("template.haml").getPath());
		template = new Template(file);
	}
	
	@Test
	public void shouldRenderCorrectlyAHamlTemplate() throws IOException {
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("name", "Teddy");
		String output = template.render(context);
		assertEquals("", "<h2>Teddy</h2>", output.trim());
	}
	
}
