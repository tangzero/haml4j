package com.github.tangzero.haml4j;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests for {@link Container}
 */
public class ContainerTest {

	@Test
	public void shouldNotBeInstantiable() {
		assertTrue("Should not have any public constructor.", Container.class.getConstructors().length == 0);
	}

	@Test
	public void shouldGetAInstanceOfContainer() {
		assertNotNull("Should return an instance of Container.", Container.getInstance());
	}

	@Test
	public void shouldReturnTheSameInstance() {
		assertEquals("Should always return the same instance.", Container.getInstance(), Container.getInstance());
	}

	@Test
	public void shouldRunAScript() {
		String result = (String) Container.getInstance().run("hello.rb");
		assertEquals("Should run a script correctly.", "Hello, Teddy.", result);
	}
	
	@Test
	public void shouldCallAMethodOfAObject() {
		Container container = Container.getInstance();
		Object string = container.run("simple_string.rb");
		String result = container.call(string, "*", 3);
		assertEquals("Should call a mehod of object.", "FakeFakeFake", result);
	}

}
